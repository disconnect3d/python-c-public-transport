__author__ = 'dc'

from psycopg2 import ProgrammingError
from flask import current_app as app
from flask.ext import restful
from utils import get_db


class DBInit(restful.Resource):
    def post(self):
        with get_db() as c:
            return self.initialize_db(c)

    @staticmethod
    def initialize_db(cursor):
        return_code = 0
        msg = "Database initialization succeeded."
        try:
            with open("public_transport/initialize_database.sql", "r", encoding='utf-8') as fp:
                cursor.execute(fp.read())
        except ProgrammingError as e:
            msg = str(e)
            return_code = -1

        return {"return_code": return_code, "msg": msg}


class DBStats(restful.Resource):
    def get(self):
        with get_db() as c:
            c.execute("SELECT * FROM get_tables_stats()")
            return c.many_results()


class DBReset(restful.Resource):
    def put(self):
        with get_db() as c:
            try:
                c.execute("DROP SCHEMA public_transport CASCADE")
            except ProgrammingError:    # occurs when schema does not exist
                pass
            c.execute("CREATE SCHEMA public_transport")
            c.execute("ALTER USER {} SET search_path TO public_transport".format(app.config['DB_CONFIG']['user']))
            c.connection.commit()

            result = DBInit.initialize_db(c)
            if result['return_code']:
                return result

            return DBFillExampleData.fill_data(c)


class DBFillExampleData(restful.Resource):
    def post(self):
        with get_db() as c:
            return self.fill_data(c)

    @staticmethod
    def fill_data(cursor):
        return_code = 0
        msg = "Example data inserting succeeded."
        try:
            with open("public_transport/fill_database.sql", "r", encoding='utf-8') as fp:
                cursor.execute(fp.read())
        except ProgrammingError as e:
            return_code = -1
            msg = str(e)

        return {"return_code": return_code, "msg": msg}
