__author__ = 'dc'

from flask.ext import restful
from flask.ext.restful import reqparse

from public_transport.resource_factory import get_resource_classes
from utils import get_db, handle_errors


Vehicle, VehicleList = get_resource_classes(
    table='vehicle',
    fields=[
        ('production_date', (str, True)),
        ('license_plate',   (str, True)),
        ('last_inspection', (str, True)),  # Date field
        ('next_inspection', (str, False))   # Date field
    ]
)

Driver, DriverList = get_resource_classes(
    table='driver',
    fields=[
        ('name',        (str,   True)),
        ('surname',     (str,   True)),
        ('employ_date', (str, True)),       # Date field
    ]
)

Line, LineList = get_resource_classes(
    table='line',
    fields=[
        ('id', (str, True))
    ]
)

DayType, DayTypeList = get_resource_classes(
    table='day_type',
    fields=[
        ('name',        (str,    True))
    ]
)

Stop, StopList = get_resource_classes(
    table='stop',
    fields=[
        ('street_id',   (int,    True)),
        ('stop_name',   (str,    True))
    ]
)

Street, StreetList = get_resource_classes(
    table='street',
    fields=[
        ('street_name', (str,    True))
    ]
)

Ride, RideList = get_resource_classes(
    table='ride',
    fields=[
        ('line_id',     (int,    True)),
        ('day_type_id', (int,    True)),
        ('vehicle_id',  (int,    True)),
        ('driver_id',   (int,    True)),
        ('direction',   (bool,    True)),
        ('ride_number', (int,    False))
    ]
)

Schedule, ScheduleList = get_resource_classes(
    table='schedule',
    fields=[
        ('ride_id',             (int,   True)),
        ('stop_id',             (int,   True)),
        ('departure_time',      (str,   True)),     # TimeField ...
        ('stop_counter',        (int,   True)),

        ('ride_line_id',        (int,   False)),
        ('ride_day_type_id',    (int,   False)),
        ('ride_vehicle_id',     (int,   False)),
        ('ride_driver_id',      (int,   False)),

        ('stop_street_id',      (int,   False))
    ]
)

args_parser = reqparse.RequestParser()
args_parser.add_argument('line_id', type=int, required=True)
args_parser.add_argument('stop_id', type=int, required=True)
args_parser.add_argument('stop_order', type=int, required=True)


class LineHasStop(restful.Resource):
    @handle_errors
    def delete(self, line_id, stop_id):
        sql_str = "DELETE FROM line_has_stop WHERE line_id=%s AND stop_id=%s"
        sql_args = [line_id, stop_id]

        with get_db() as c:
            c.execute(sql_str, sql_args)
            return '', 204


class LineHasStopList(restful.Resource):
    @handle_errors
    def get(self):
        with get_db() as c:
            c.execute('SELECT * FROM line_has_stop')
            return c.many_results()

    @handle_errors
    def post(self):
        args = args_parser.parse_args()

        sql_str = 'INSERT INTO line_has_stop (line_id, stop_id, stop_order) VALUES (%s, %s, %s)'
        sql_args = [args['line_id'], args['stop_id'], args['stop_order']]

        with get_db() as c:
            c.execute(sql_str, sql_args)
            return args, 201


class RideForLineList(restful.Resource):
    @handle_errors
    def get(self, line_id):
        with get_db() as c:
            c.execute('SELECT * FROM ride WHERE line_id=%s', [line_id])
            return c.many_results()


class ScheduleForLineList(restful.Resource):
    @handle_errors
    def get(self, line_id, day_type_id, direction):
        with get_db() as c:
            c.execute('SELECT * FROM departure_times_for_line(%s, %s, %s)',
                      [line_id, day_type_id, True if direction else False])
            return c.many_results()


class DriverStatistics(restful.Resource):
    @handle_errors
    def get(self):
        with get_db() as c:
            c.execute('select driver_str, day_type, rides_count from get_drivers_statistics()')
            return c.many_results()


class SearchLineByStop(restful.Resource):
    @handle_errors
    def get(self, stop_id_from, stop_id_to, day_type_id):
        with get_db() as c:
            c.execute('select * from find_line_by_stops(%s, %s, %s)', [stop_id_from, stop_id_to, day_type_id])
            return c.many_results()