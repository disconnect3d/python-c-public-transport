__author__ = 'dc'

from collections import OrderedDict
from flask import request
from flask.ext import restful
from flask.ext.restful import reqparse
from utils import get_db, handle_errors


def log(*s):
    print(*s)


def get_resource_classes(table, fields, **kwargs):
    rcf = ResourceClassFactory(table, fields, **kwargs)
    return rcf.get_resource(), rcf.get_list_resource()


def get_resource_list_class(table, fields, **kwargs):
    rcf = ResourceClassFactory(table, fields, **kwargs)
    return rcf.get_list_resource()


class ResourceClassFactory(object):
    def __init__(self, table, fields, **kwargs):
        self.table = table
        self.fields = OrderedDict(fields)
        self.fields_list = ', '.join((field_name for field_name in self.fields))
        self.get_fields_list = ', '.join((field_name for field_name in ['id'] + list(self.fields.keys())))
        self.kwargs = kwargs

        self.args_parser = reqparse.RequestParser()
        for key in self.fields:
            self.args_parser.add_argument(key, type=self.fields[key][0], required=self.fields[key][1], location='json')

    def get_resource(self):
        this = self

        class Resource(restful.Resource):
            @handle_errors
            def get(self, id):
                with get_db() as c:
                    c.execute("SELECT {} FROM {} WHERE id=%s".format(this.fields_list, this.table), [id])
                    return c.result()

            @handle_errors
            def delete(self, id):
                try:
                    cascade = True if request.args.get('cascade', '') else False
                except KeyError:
                    cascade = False

                sql_str = "DELETE FROM {} WHERE id=%s".format(this.table)

                if cascade:
                    sql_str += " CASCADE"

                print(sql_str) # TODO: Del me

                with get_db() as c:
                    c.execute(sql_str, [id])
                    return '', 204

            @handle_errors
            def put(self, id):
                args = this.args_parser.parse_args()

                with get_db() as c:
                    c.execute("UPDATE {} SET {}=%s WHERE id=%s".format(this.table, '=%s, '.join((field_name for field_name in this.fields))),
                              [args[field] for field in this.fields] + [id])
                    return '', 205

        return Resource

    def get_list_resource(self):
        this = self

        class PostListResource(restful.Resource):
            @handle_errors
            def post(self):
                args = this.args_parser.parse_args()
                log('POST:', args)

                sql_str = "INSERT INTO {} ({}) VALUES ({})".format(this.table, this.fields_list, ('%s, ' * len(this.fields))[:-2])
                sql_args = [args[field] for field in this.fields.keys()]

                log(sql_str, '%', sql_args)

                with get_db() as c:
                    c.execute(sql_str, sql_args)
                    return args, 201

        if self.kwargs.pop('list_resource_only_get', None):
            list_resource_base_class = restful.Resource
        else:
            list_resource_base_class = PostListResource

        class ListResource(list_resource_base_class):
            @handle_errors
            def get(self):
                sql_str = "SELECT {} FROM {}".format(this.get_fields_list, this.table)

                log(sql_str)

                with get_db() as c:
                    c.execute(sql_str)
                    result = c.many_results()
                    log(result)
                    return result

        return ListResource
