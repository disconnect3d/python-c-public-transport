INSERT INTO day_type (name) VALUES
  ('dni robocze'),
  ('weekendy i święta');


INSERT INTO driver (name, surname, employ_date) VALUES ('Jan', 'Kowalski', '2014-05-01');
INSERT INTO driver (name, surname, employ_date) VALUES ('Paweł', 'Trąba', '2014-01-01');
INSERT INTO driver (name, surname, employ_date) VALUES ('Kierowca', 'Autobusu', '2014-09-25');
INSERT INTO driver (name, surname, employ_date) VALUES ('Dominik', 'Bazodanowy', '2015-01-01');
INSERT INTO driver (name, surname, employ_date) VALUES ('Pracownik', 'Roku', '2007-07-07');
INSERT INTO driver (name, surname, employ_date) VALUES ('Patryk', 'Zielinski', '2009-02-03');


INSERT INTO line (id) VALUES (100);


INSERT INTO street (street_name) VALUES ('Salwator');
INSERT INTO street (street_name) VALUES ('Malczewskiego');
INSERT INTO street (street_name) VALUES ('Aleja Waszyngtona');
INSERT INTO street (street_name) VALUES ('Kopiec Kościuszki');


INSERT INTO stop (street_id, stop_name) VALUES (1, 'Salwator');
INSERT INTO stop (street_id, stop_name) VALUES (2, 'Malczewskiego');
INSERT INTO stop (street_id, stop_name) VALUES (3, 'Aleja Waszyngtona');
INSERT INTO stop (street_id, stop_name) VALUES (4, 'Kopiec Kościuszki');


INSERT INTO vehicle (production_date, license_plate, last_inspection, next_inspection) VALUES ('2004-05-01', 'ABC-1234', '2014-04-15', '2015-04-15');
INSERT INTO vehicle (production_date, license_plate, last_inspection, next_inspection) VALUES ('2009-02-02', 'XYZ-9876', '2014-05-07', '2015-05-07');
INSERT INTO vehicle (production_date, license_plate, last_inspection, next_inspection) VALUES ('2008-01-11', 'DEF-5678', '2014-08-05', '2015-08-05');
INSERT INTO vehicle (production_date, license_plate, last_inspection, next_inspection) VALUES ('2000-09-29', 'QWE-6543', '2015-01-01', '2016-01-01');
INSERT INTO vehicle (production_date, license_plate, last_inspection, next_inspection) VALUES ('1990-06-25', 'MNO-3312', '2015-01-04', '2016-01-04');


INSERT INTO ride (line_id, day_type_id, vehicle_id, driver_id, ride_number, direction) VALUES (100, 1, 1, 1, 1, true);
INSERT INTO ride (line_id, day_type_id, vehicle_id, driver_id, ride_number, direction) VALUES (100, 1, 1, 1, 2, true);
INSERT INTO ride (line_id, day_type_id, vehicle_id, driver_id, ride_number, direction) VALUES (100, 1, 1, 2, 3, true);
INSERT INTO ride (line_id, day_type_id, vehicle_id, driver_id, ride_number, direction) VALUES (100, 1, 2, 3, 1, false);
INSERT INTO ride (line_id, day_type_id, vehicle_id, driver_id, ride_number, direction) VALUES (100, 1, 2, 3, 2, false);
INSERT INTO ride (line_id, day_type_id, vehicle_id, driver_id, ride_number, direction) VALUES (100, 1, 2, 3, 3, false);


INSERT INTO line_has_stop (line_id, stop_id, stop_order) VALUES (100, 1, 1);
INSERT INTO line_has_stop (line_id, stop_id, stop_order) VALUES (100, 2, 2);
INSERT INTO line_has_stop (line_id, stop_id, stop_order) VALUES (100, 3, 3);
INSERT INTO line_has_stop (line_id, stop_id, stop_order) VALUES (100, 4, 4);


INSERT INTO schedule (ride_id, stop_id, departure_time, stop_counter) VALUES (1, 1, '07:05:00', 1);
INSERT INTO schedule (ride_id, stop_id, departure_time, stop_counter) VALUES (1, 2, '07:10:00', 2);
INSERT INTO schedule (ride_id, stop_id, departure_time, stop_counter) VALUES (1, 3, '07:20:00', 3);
INSERT INTO schedule (ride_id, stop_id, departure_time, stop_counter) VALUES (1, 4, '07:50:00', 4);
INSERT INTO schedule (ride_id, stop_id, departure_time, stop_counter) VALUES (2, 1, '08:05:00', 1);
INSERT INTO schedule (ride_id, stop_id, departure_time, stop_counter) VALUES (2, 2, '08:10:00', 2);
INSERT INTO schedule (ride_id, stop_id, departure_time, stop_counter) VALUES (2, 3, '08:20:00', 3);
INSERT INTO schedule (ride_id, stop_id, departure_time, stop_counter) VALUES (2, 4, '08:50:00', 4);
INSERT INTO schedule (ride_id, stop_id, departure_time, stop_counter) VALUES (3, 1, '09:05:00', 1);
INSERT INTO schedule (ride_id, stop_id, departure_time, stop_counter) VALUES (3, 2, '09:10:00', 2);
INSERT INTO schedule (ride_id, stop_id, departure_time, stop_counter) VALUES (3, 3, '09:20:00', 3);
INSERT INTO schedule (ride_id, stop_id, departure_time, stop_counter) VALUES (3, 4, '09:50:00', 4);
INSERT INTO schedule (ride_id, stop_id, departure_time, stop_counter) VALUES (4, 4, '07:25:00', 1);
INSERT INTO schedule (ride_id, stop_id, departure_time, stop_counter) VALUES (4, 3, '07:30:00', 2);
INSERT INTO schedule (ride_id, stop_id, departure_time, stop_counter) VALUES (4, 2, '07:35:00', 3);
INSERT INTO schedule (ride_id, stop_id, departure_time, stop_counter) VALUES (4, 1, '07:45:00', 4);
INSERT INTO schedule (ride_id, stop_id, departure_time, stop_counter) VALUES (5, 4, '08:25:00', 1);
INSERT INTO schedule (ride_id, stop_id, departure_time, stop_counter) VALUES (5, 3, '08:30:00', 2);
INSERT INTO schedule (ride_id, stop_id, departure_time, stop_counter) VALUES (5, 2, '08:35:00', 3);
INSERT INTO schedule (ride_id, stop_id, departure_time, stop_counter) VALUES (5, 1, '08:45:00', 4);
INSERT INTO schedule (ride_id, stop_id, departure_time, stop_counter) VALUES (6, 4, '09:20:00', 1);
INSERT INTO schedule (ride_id, stop_id, departure_time, stop_counter) VALUES (6, 3, '09:25:00', 2);
INSERT INTO schedule (ride_id, stop_id, departure_time, stop_counter) VALUES (6, 2, '09:30:00', 3);
INSERT INTO schedule (ride_id, stop_id, departure_time, stop_counter) VALUES (6, 1, '09:45:00', 4);
