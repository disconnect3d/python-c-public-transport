CREATE TABLE line (
  id SERIAL  NOT NULL ,
PRIMARY KEY(id));


CREATE DOMAIN date_t AS DATE NOT NULL CHECK (VALUE <= CURRENT_DATE);

CREATE TABLE vehicle (
  id SERIAL ,
  production_date date_t ,
  license_plate VARCHAR   NOT NULL UNIQUE ,
  last_inspection date_t  CHECK (last_inspection >= production_date),
  next_inspection DATE   DEFAULT NULL CHECK (next_inspection > last_inspection) ,
PRIMARY KEY(id));

CREATE OR REPLACE FUNCTION vehicle_insert_update() RETURNS TRIGGER AS '
BEGIN
  new.next_inspection = new.last_inspection + INTERVAL ''1 year'';
  RETURN new;
END;
' LANGUAGE 'plpgsql';

CREATE TRIGGER vehicle_insert_update BEFORE INSERT OR UPDATE ON vehicle
  FOR EACH ROW EXECUTE PROCEDURE vehicle_insert_update();



CREATE TABLE street (
  id SERIAL  NOT NULL ,
  street_name VARCHAR   NOT NULL  UNIQUE  ,
PRIMARY KEY(id));



CREATE TABLE day_type (
  id SERIAL  NOT NULL ,
  name VARCHAR   NOT NULL  UNIQUE ,
PRIMARY KEY(id));




CREATE TABLE driver (
  id SERIAL NOT NULL ,
  name VARCHAR   NOT NULL ,
  surname VARCHAR   NOT NULL ,
  employ_date DATE   NOT NULL CHECK (employ_date <= CURRENT_TIMESTAMP) ,
PRIMARY KEY(id));



CREATE TABLE stop (
  id SERIAL   NOT NULL ,
  street_id INTEGER   NOT NULL ,
  stop_name VARCHAR   NOT NULL   ,
PRIMARY KEY(id, street_id)  ,
  FOREIGN KEY(street_id)
    REFERENCES street(id)
    ON UPDATE CASCADE);



CREATE INDEX stop_FKIndex1 ON stop (street_id);


CREATE INDEX IFK_Rel_01 ON stop (street_id);

CREATE DOMAIN order_t AS INTEGER NOT NULL CHECK (VALUE > 0);
CREATE TABLE line_has_stop (
  line_id INTEGER   NOT NULL ,
  stop_street_id INTEGER   NOT NULL ,
  stop_id INTEGER   NOT NULL ,
  stop_order order_t ,
PRIMARY KEY(line_id, stop_street_id, stop_id)    ,
  FOREIGN KEY(line_id)
    REFERENCES line(id)
    ON UPDATE CASCADE,
  FOREIGN KEY(stop_id, stop_street_id)
    REFERENCES stop(id, street_id)
    ON UPDATE CASCADE);

CREATE OR REPLACE FUNCTION line_has_stop_insert_update() RETURNS TRIGGER AS '
BEGIN
  SELECT street_id FROM stop WHERE id=new.stop_id INTO new.stop_street_id;
  RETURN new;
END;
' LANGUAGE 'plpgsql';

CREATE TRIGGER line_has_stop_insert_update BEFORE INSERT OR UPDATE ON line_has_stop
  FOR EACH ROW EXECUTE PROCEDURE line_has_stop_insert_update();



CREATE INDEX line_has_stop_FKIndex1 ON line_has_stop (line_id);
CREATE INDEX line_has_stop_FKIndex2 ON line_has_stop (stop_id, stop_street_id);


CREATE INDEX IFK_Rel_10 ON line_has_stop (line_id);
CREATE INDEX IFK_Rel_11 ON line_has_stop (stop_id, stop_street_id);


CREATE TABLE ride (
  id SERIAL  NOT NULL ,
  line_id INTEGER   NOT NULL ,
  day_type_id INTEGER   NOT NULL ,
  vehicle_id INTEGER   NOT NULL ,
  driver_id INTEGER   NOT NULL ,
  ride_number order_t ,
  direction BOOLEAN NOT NULL ,
PRIMARY KEY(id, line_id, day_type_id, vehicle_id, driver_id)        ,
  FOREIGN KEY(line_id)
    REFERENCES line(id),
  FOREIGN KEY(day_type_id)
    REFERENCES day_type(id),
  FOREIGN KEY(vehicle_id)
    REFERENCES vehicle(id),
  FOREIGN KEY(driver_id)
    REFERENCES driver(id)
    ON UPDATE CASCADE
);

CREATE OR REPLACE FUNCTION ride_before_insert_update() RETURNS TRIGGER AS '
DECLARE
  ride_num INTEGER;
BEGIN
  SELECT MAX(ride_number) FROM ride WHERE line_id=new.line_id AND day_type_id=new.day_type_id INTO ride_num;

  IF new.ride_number IS NULL THEN
    IF ride_num IS NULL THEN
      ride_num = 0;
    END IF;

    new.ride_number = ride_num + 1;
  END IF;

  RETURN new;
END;
' LANGUAGE 'plpgsql';

CREATE TRIGGER ride_before_insert_update BEFORE INSERT OR UPDATE ON ride
  FOR EACH ROW EXECUTE PROCEDURE ride_before_insert_update();



CREATE INDEX ride_FKIndex1 ON ride (line_id);
CREATE INDEX ride_FKIndex2 ON ride (day_type_id);
CREATE INDEX ride_FKIndex3 ON ride (vehicle_id);
CREATE INDEX ride_FKIndex4 ON ride (driver_id);


CREATE INDEX IFK_Rel_04 ON ride (line_id);
CREATE INDEX IFK_Rel_05 ON ride (day_type_id);
CREATE INDEX IFK_Rel_06 ON ride (vehicle_id);
CREATE INDEX IFK_Rel_07 ON ride (driver_id);


CREATE TABLE schedule (
  id SERIAL  NOT NULL ,
  ride_id INTEGER   NOT NULL ,
  ride_line_id INTEGER ,
  ride_day_type_id INTEGER ,
  ride_vehicle_id INTEGER ,
  ride_driver_id INTEGER ,
  stop_street_id INTEGER ,
  stop_id INTEGER   NOT NULL ,
  departure_time TIME   NOT NULL ,
  stop_counter order_t  ,
PRIMARY KEY(id, ride_id, ride_line_id, ride_day_type_id, ride_vehicle_id, ride_driver_id, stop_street_id, stop_id)    ,
  FOREIGN KEY(ride_line_id, ride_id, ride_day_type_id, ride_vehicle_id, ride_driver_id)
    REFERENCES ride(line_id, id, day_type_id, vehicle_id, driver_id)
    ON UPDATE CASCADE,
  FOREIGN KEY(stop_id, stop_street_id)
    REFERENCES stop(id, street_id)
    ON UPDATE CASCADE);

CREATE OR REPLACE FUNCTION schedule_insert_update() RETURNS TRIGGER AS '
DECLARE
  r_line_id    INTEGER;
  r_dtype_id   INTEGER;
  r_vehicle_id INTEGER;
  r_driver_id  INTEGER;

  sorder       INTEGER;
  stops_count  INTEGER;
BEGIN
/* Updating foreign keys */
  new.stop_street_id = (SELECT street_id FROM stop WHERE id = new.stop_id);

  SELECT line_id, day_type_id, vehicle_id, driver_id FROM ride WHERE id = new.ride_id
    INTO r_line_id, r_dtype_id, r_vehicle_id, r_driver_id;

  new.ride_line_id = r_line_id;
  new.ride_day_type_id = r_dtype_id;
  new.ride_vehicle_id = r_vehicle_id;
  new.ride_driver_id = r_driver_id;


/* Checking if stop_id is in line_has_stop for current line_id */
  SELECT INTO sorder stop_order FROM line_has_stop WHERE line_id=new.ride_line_id AND stop_id=new.stop_id;
  IF sorder IS NOT NULL THEN
    SELECT INTO stops_count COUNT(*) FROM line_has_stop WHERE line_id=new.ride_line_id;

    IF new.stop_counter <> sorder AND new.stop_counter <> stops_count - sorder + 1 THEN
      RAISE EXCEPTION ''The % stop_counter for % stop_id does not match either for ascending and descending stop_order in line_has_stop for % line_id'',
                      new.stop_counter, new.stop_id, new.ride_line_id;
    END IF;
  ELSE
    RAISE EXCEPTION ''The % stop_id does not exist in line_has_stop for % line_id'', new.stop_id, new.ride_line_id;
  END IF;

  RETURN new;
END;
' LANGUAGE 'plpgsql';

CREATE TRIGGER schedule_insert_update BEFORE INSERT OR UPDATE ON schedule
  FOR EACH ROW EXECUTE PROCEDURE schedule_insert_update();

CREATE INDEX schedule_FKIndex1 ON schedule (ride_line_id, ride_id, ride_day_type_id, ride_vehicle_id, ride_driver_id);
CREATE INDEX schedule_FKIndex2 ON schedule (stop_id, stop_street_id);


CREATE INDEX IFK_Rel_02 ON schedule (ride_line_id, ride_id, ride_day_type_id, ride_vehicle_id, ride_driver_id);
CREATE INDEX IFK_Rel_12 ON schedule (stop_id, stop_street_id);

CREATE VIEW schedule_with_direction AS SELECT s.*, r.direction FROM schedule s JOIN ride r ON s.ride_id=r.id;

CREATE VIEW line_with_stops AS SELECT l.line_id, l.stop_id, s.stop_name, l.stop_order FROM line_has_stop l JOIN stop s ON s.id=l.stop_id;
CREATE VIEW line_with_stops_asc AS SELECT * FROM line_with_stops ORDER BY stop_order ASC;
CREATE VIEW line_with_stops_desc AS SELECT * FROM line_with_stops ORDER BY stop_order DESC;

CREATE TYPE schedule_for_line AS (
  stop_count INTEGER,
  stop_name VARCHAR,
  departure_times VARCHAR
);

CREATE FUNCTION get_departure_times_for_stop(_stop_id INTEGER, _day_type_id INTEGER, _direction BOOLEAN) RETURNS VARCHAR AS '
DECLARE
  dep_time TIME;
  result VARCHAR;
BEGIN
  result = '''';

  FOR dep_time in SELECT departure_time FROM schedule_with_direction WHERE stop_id=_stop_id AND ride_day_type_id=_day_type_id AND direction=_direction ORDER BY departure_time
  LOOP
    result = result || TO_CHAR(dep_time, ''HH24:MI'') || '' '';
  END LOOP;

  IF result='''' THEN
    RAISE EXCEPTION ''There are no departures for % stop_id, % day_type_id and % direction'', _stop_id, _day_type_id, _direction;
  END IF;

  RETURN substring(result FOR CHAR_LENGTH(result)-1);
END;
' LANGUAGE 'plpgsql';


CREATE FUNCTION departure_times_for_line(lineid INTEGER, daytypeid INTEGER, direction BOOLEAN)
  RETURNS SETOF schedule_for_line AS '
DECLARE
  ordered_line_with_stops VARCHAR;
  stopid INTEGER;
  stopname VARCHAR;
  dep_times VARCHAR;
  counter INTEGER;
BEGIN
  IF direction IS TRUE THEN
    ordered_line_with_stops = ''line_with_stops_asc'';
  ELSE
    ordered_line_with_stops = ''line_with_stops_desc'';
  END IF;

  counter = 0;
  FOR stopid, stopname IN EXECUTE format(''SELECT stop_id, stop_name FROM %s WHERE line_id=%s'', ordered_line_with_stops, lineid)
  LOOP
    counter = counter + 1;
    SELECT * FROM get_departure_times_for_stop(stopid, daytypeid, direction) INTO dep_times;
    RETURN NEXT (counter, stopname, dep_times);
  END LOOP;
END;
' LANGUAGE 'plpgsql';


/**********************************************************************/
/* Used by DBStats.get ( GET /db/stats ) for returning SQL statistics */
/**********************************************************************/

CREATE TYPE sql_stats_type AS (
  table_name information_schema.sql_identifier,
  records_count INTEGER
);

CREATE FUNCTION get_tables_stats() RETURNS SETOF sql_stats_type AS '
DECLARE
  tname information_schema.sql_identifier;
  records_count integer;
BEGIN
  FOR tname IN SELECT table_name FROM information_schema.tables WHERE table_schema = ''public_transport'' AND table_catalog = ''public_transport'' AND table_type=''BASE TABLE'' ORDER BY table_name
  LOOP
    EXECUTE format(''SELECT COUNT(*) FROM %s'', tname) INTO records_count;
    RETURN NEXT (tname, records_count);
  END LOOP;
END;
' LANGUAGE 'plpgsql';

CREATE TYPE driver_stats_type AS (
  driver_str TEXT,
  day_type VARCHAR,
  rides_count BIGINT
);
CREATE FUNCTION get_drivers_statistics() RETURNS SETOF driver_stats_type AS '
DECLARE
  day_type_iter_id INTEGER;
  day_type_iter_name VARCHAR;
  record_iter RECORD;
BEGIN
  FOR day_type_iter_id, day_type_iter_name IN SELECT id, name FROM day_type
  LOOP
    FOR record_iter IN SELECT d.name || '' '' || d.surname as driver_fullname, COUNT(r.driver_id) rides_count FROM driver d LEFT JOIN ride r ON d.id = r.driver_id WHERE r.day_type_id=day_type_iter_id GROUP BY d.name, d.surname
    LOOP
      RETURN NEXT (record_iter.driver_fullname, day_type_iter_name, record_iter.rides_count);
    END LOOP;
  END LOOP;
END;
' LANGUAGE 'plpgsql';


CREATE VIEW schedule_with_daytype_name AS SELECT s.*, d.name FROM schedule s JOIN day_type d ON s.ride_day_type_id=d.id;

CREATE TYPE line_finder_result_type AS (
  line_id INTEGER,
  from_time TIME,
  to_time TIME
);

CREATE FUNCTION find_line_by_stops(stop_id_from INTEGER, stop_id_to INTEGER, day_type_id_arg INTEGER) RETURNS SETOF line_finder_result_type AS '
DECLARE
  line_id_iter INTEGER;
  ride_id_iter INTEGER;
  schedule_from_time TIME;
  schedule_to_time TIME;
BEGIN
  FOR line_id_iter IN SELECT DISTINCT line_id FROM line_has_stop WHERE stop_id IN (stop_id_from, stop_id_to) LOOP
    FOR ride_id_iter IN SELECT id FROM ride WHERE line_id=line_id_iter LOOP
      SELECT departure_time FROM schedule_with_daytype_name WHERE ride_id=ride_id_iter AND stop_id=stop_id_from AND ride_day_type_id=day_type_id_arg INTO schedule_from_time;
      SELECT departure_time FROM schedule_with_daytype_name WHERE ride_id=ride_id_iter AND stop_id=stop_id_to AND ride_day_type_id=day_type_id_arg INTO schedule_to_time;

      IF schedule_from_time < schedule_to_time THEN
        RETURN NEXT (line_id_iter, schedule_from_time, schedule_to_time);
      END IF;

    END LOOP;
  END LOOP;
END;
' LANGUAGE 'plpgsql';