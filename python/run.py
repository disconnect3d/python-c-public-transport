from flask import Flask
from flask.ext.restful import Api

from public_transport.api_db import DBInit, DBStats, DBReset, DBFillExampleData
from public_transport.api_resources import \
    Vehicle, Stop, Street, Driver, DayType, Line, Ride, Schedule, \
    VehicleList, StopList, StreetList, DriverList, DayTypeList, LineList, RideList, ScheduleList, \
    LineHasStop, LineHasStopList, ScheduleForLineList, RideForLineList, \
    DriverStatistics, SearchLineByStop

app = Flask(__name__)
api = Api(app)

app.config.from_object('config')

api.add_resource(DBInit, '/db/init')
api.add_resource(DBStats, '/db/stats')
api.add_resource(DBReset, '/db/reset')
api.add_resource(DBFillExampleData, '/db/fill')

api.add_resource(Vehicle, '/api/vehicle/<int:id>', endpoint='vehicle')
api.add_resource(VehicleList, '/api/vehicle', endpoint='vehicle_list')

api.add_resource(Driver, '/api/driver/<int:id>', endpoint='driver')
api.add_resource(DriverList, '/api/driver', endpoint='driver_list')

api.add_resource(DayType, '/api/day_type/<int:id>', endpoint='day_type')
api.add_resource(DayTypeList, '/api/day_type', endpoint='day_type_list')

api.add_resource(Line, '/api/line/<int:id>', endpoint='line')
api.add_resource(LineList, '/api/line', endpoint='line_list')

api.add_resource(Stop, '/api/stop/<int:id>', endpoint='stop')
api.add_resource(StopList, '/api/stop', endpoint='stop_list')

api.add_resource(Street, '/api/street/<int:id>', endpoint='street')
api.add_resource(StreetList, '/api/street', endpoint='street_list')

api.add_resource(Ride, '/api/ride/<int:id>', endpoint='ride')
api.add_resource(RideList, '/api/ride', endpoint='ride_list')

api.add_resource(Schedule, '/api/schedule/<int:id>', endpoint='schedule')
api.add_resource(ScheduleList, '/api/schedule', endpoint='schedule_list')

api.add_resource(LineHasStop, '/api/line_has_stop/<int:line_id>/<int:stop_id>')
api.add_resource(LineHasStopList, '/api/line_has_stop')

api.add_resource(RideForLineList, '/api/ride_for_line/<int:line_id>')

api.add_resource(ScheduleForLineList, '/api/schedule_for_line/<int:line_id>/<int:day_type_id>/<int:direction>')

api.add_resource(DriverStatistics, '/api/driver_stats')

api.add_resource(SearchLineByStop, '/api/search_line/<int:stop_id_from>/<int:stop_id_to>/<int:day_type_id>')

if __name__ == "__main__":
    if app.config['DEBUG']:
        app.run(debug=True, host='0.0.0.0')
    else:                       # production only!
        app.run(host='0.0.0.0')
