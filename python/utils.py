__author__ = 'dc'

from flask import g
from flask import current_app as app

from sql import DBHandler


def get_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    if not hasattr(g, 'db'):
        g.db = DBHandler(**app.config['DB_CONFIG'])
    return g.db


def handle_errors(get_method):
    def wrapper(*args, **kwargs):
        try:
            result = get_method(*args, **kwargs)
            return result
        except Exception as e:

            if hasattr(e, 'cursor'):
                print("!!! Error in query: ", e.cursor.query, '\n', e, sep='')
                return {"query": e.cursor.query.decode('utf-8'), "error": str(e)}
            else:
                err_msg = str(e)
                if hasattr(e, 'data'):
                    err_msg += '\n' + e.data['message'] if 'message'in e.data else ''
                print("Error: ", err_msg)
                return {"error": err_msg}
    return wrapper
