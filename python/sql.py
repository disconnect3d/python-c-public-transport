import psycopg2


class CursorWrapper(object):
    def __init__(self, connection, cursor):
        self.connection = connection
        self.cursor = cursor

    def __getattr__(self, item):
        """In order to provide the same interface as Cursor object has"""
        return getattr(self.cursor, item)

    def many_results(self):
        """Returns all rows from a cursor as a list of dicts (prepared for json.dumps)"""
        if self.cursor.rowcount:
            return [dict(zip([col[0] for col in self.cursor.description], map(str, row)))
                    for row in self.cursor.fetchall()]
        else:
            return []

    def result(self):
        """Returns one row from a cursor as a dict (prepared for json.dumps)"""
        if self.cursor.rowcount:
            return dict(zip([col[0] for col in self.cursor.description], self.cursor.fetchone()))
        else:
            return {}


class DBHandler(object):
    def __init__(self, host, database, user, password, port=5432):
        self.host = host
        self.port = port
        self.database = database
        self.user = user
        self.pwd = password
        self._connection = None
        self._cursor = None

    def connect(self):
        self.__connect()

    def __connect(self):
        self._connection = psycopg2.connect(
            host=self.host,
            port=self.port,
            database=self.database,
            user=self.user,
            password=self.pwd
        )

    @property
    def connection(self):
        if not self._connection or not self._connection.closed:
            self.__connect()
        return self._connection

    def cursor(self):
        self._cursor = self.connection.cursor()
        return CursorWrapper(self._connection, self._cursor)

    def __enter__(self):
        return self.cursor()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._connection.commit()
        self._cursor.close()

