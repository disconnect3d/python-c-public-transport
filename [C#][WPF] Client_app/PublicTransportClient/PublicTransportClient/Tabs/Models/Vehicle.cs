﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using Newtonsoft.Json;

namespace PublicTransportClient.Tabs.Models
{
    public class Vehicle : ModelBase
    {
        private String _ProductionDate;

        [JsonProperty(PropertyName = "production_date")]
        public String ProductionDate
        {
            get { return _ProductionDate; }
            set
            {
                _ProductionDate = value;
                RaisePropertyChanged(() => ProductionDate);
            }
        }

        private String _LicensePlate;

        [JsonProperty(PropertyName = "license_plate")]
        public String LicensePlate
        {
            get { return _LicensePlate; }
            set
            {
                _LicensePlate = value;
                RaisePropertyChanged(() => LicensePlate);
            }
        }

        private String _LastInspection;

        [JsonProperty(PropertyName = "last_inspection")]
        public String LastInspection
        {
            get { return _LastInspection; }
            set
            {
                _LastInspection = value;
                RaisePropertyChanged(() => LastInspection);
            }
        }

        private String _NextInspection;

        public String NextInspection
        {
            get { return _NextInspection; }
            set
            {
                _NextInspection = value;
                RaisePropertyChanged(() => NextInspection);
            }
        }

        public override bool IsValid()
        {
            return !(string.IsNullOrEmpty(ProductionDate) || string.IsNullOrEmpty(LastInspection) || string.IsNullOrEmpty(LicensePlate));
        }

        public override string ToString()
        {
            return LicensePlate + " (Id: " + Id + ")";
        }
    }
}
