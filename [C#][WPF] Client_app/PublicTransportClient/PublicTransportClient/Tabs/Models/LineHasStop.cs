﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using Newtonsoft.Json;

namespace PublicTransportClient.Tabs.Models
{
    public class LineHasStop : ModelBase
    {
        private int _LineId;

        [JsonProperty(PropertyName = "line_id")]
        public int LineId
        {
            get { return _LineId; }
            set
            {
                _LineId = value;
                RaisePropertyChanged(() => LineId);
            }
        }

        private int _StopId;
        [JsonProperty(PropertyName = "stop_id")]
        public int StopId
        {
            get { return _StopId; }
            set
            {
                _StopId = value;
                RaisePropertyChanged(() => StopId);
            }
        }

        private int _StopOrder;

        [JsonProperty(PropertyName = "stop_order")]
        public int StopOrder
        {
            get { return _StopOrder; }
            set
            {
                _StopOrder = value;
                RaisePropertyChanged(() => StopOrder);
            }
        }
        

        public override bool IsValid()
        {
            return true;
        }
    }
}
