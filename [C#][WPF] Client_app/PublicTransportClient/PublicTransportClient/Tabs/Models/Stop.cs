﻿using GalaSoft.MvvmLight;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublicTransportClient.Tabs.Models
{
    public class Stop : ModelBase
    {
        public Stop() 
        { }

        private String _StopName;

        [JsonProperty(PropertyName = "stop_name")]
        public String StopName
        {
            get { return _StopName; }
            set
            {
                _StopName = value;
                RaisePropertyChanged(() => StopName);
            }
        }


        private int _StreetId;

        [JsonProperty(PropertyName = "street_id")]
        public int StreetId
        {
            get { return _StreetId; }
            set
            {
                _StreetId = value;
                RaisePropertyChanged(() => StreetId);
            }
        }

        public override bool IsValid()
        {
            return !string.IsNullOrEmpty(StopName);
        }

        public override string ToString()
        {
            return StopName + " (Id: " + Id + ")";
        }
    }
}
