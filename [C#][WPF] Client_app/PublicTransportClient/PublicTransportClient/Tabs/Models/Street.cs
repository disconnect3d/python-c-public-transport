﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using Newtonsoft.Json;
using RestSharp.Deserializers;
using RestSharp.Serializers;

namespace PublicTransportClient.Tabs.Models
{
    public class Street : ModelBase
    {
        private String _Name;

        [JsonProperty(PropertyName = "street_name")]
        public String StreetName
        {
            get { return _Name; }
            set
            {
                _Name = value;
                RaisePropertyChanged(() => StreetName);
            }
        }

        public override bool IsValid()
        {
            return !string.IsNullOrEmpty(StreetName);
        }
    }
}
