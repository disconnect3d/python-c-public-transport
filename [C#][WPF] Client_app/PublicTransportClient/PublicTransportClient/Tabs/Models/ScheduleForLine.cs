﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using Newtonsoft.Json;

namespace PublicTransportClient.Tabs.Models
{
    public class ScheduleForLine : ObservableObject
    {
        private int _StopCount;

        public int StopCount
        {
            get { return _StopCount; }
            set
            {
                _StopCount = value;
                RaisePropertyChanged(() => StopCount);
            }
        }
        
        

        private String _StopName;

        public String StopName
        {
            get { return _StopName; }
            set
            {
                _StopName = value;
                RaisePropertyChanged(() => StopName);
            }
        }

        private String _DepartureTimes;

        public String DepartureTimes
        {
            get { return _DepartureTimes; }
            set
            {
                _DepartureTimes = value;
                RaisePropertyChanged(() => DepartureTimes);
            }
        }
        
    }
}
