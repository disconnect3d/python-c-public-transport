﻿using Newtonsoft.Json;

namespace PublicTransportClient.Tabs.Models
{
    public class Schedule : ModelBase
    {
        private int _RideId;
        
        [JsonProperty(PropertyName = "ride_id")]
        public int RideId
        {
            get { return _RideId; }
            set
            {
                _RideId = value;
                RaisePropertyChanged(() => RideId);
            }
        }

        private int _StopId;

        [JsonProperty(PropertyName = "stop_id")]
        public int StopId
        {
            get { return _StopId; }
            set
            {
                _StopId = value;
                RaisePropertyChanged(() => StopId);
            }
        }

        private string _DepartureTime;

        [JsonProperty(PropertyName = "departure_time")]
        public string DepartureTime
        {
            get { return _DepartureTime; }
            set
            {
                _DepartureTime = value;
                RaisePropertyChanged(() => DepartureTime);
            }
        }

        private int _StopCounter;

        [JsonProperty(PropertyName = "stop_counter")]
        public int StopCounter
        {
            get { return _StopCounter; }
            set
            {
                _StopCounter = value;
                RaisePropertyChanged(() => StopCounter);
            }
        }
        

        public override bool IsValid()
        {
            return true;
        }
    }
}
