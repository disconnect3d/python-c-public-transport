﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using Newtonsoft.Json;
using RestSharp.Deserializers;
using RestSharp.Serializers;

namespace PublicTransportClient.Tabs.Models
{
    public class Driver : ModelBase
    {
        private String _Name;

        [JsonProperty(PropertyName = "name")]
        public String Name
        {
            get { return _Name; }
            set
            {
                _Name = value;
                RaisePropertyChanged(() => Name);
            }
        }



        private String _Surname;

        [JsonProperty(PropertyName = "surname")]
        public String Surname
        {
            get { return _Surname; }
            set
            {
                _Surname = value;
                RaisePropertyChanged(() => Surname);
            }
        }

        private String _EmployDate;

        [JsonProperty(PropertyName = "employ_date")]
        public String EmployDate
        {
            get { return _EmployDate; }
            set
            {
                _EmployDate = value;
                RaisePropertyChanged(() => EmployDate);
            }
        }

        public override bool IsValid()
        {
            return !(string.IsNullOrEmpty(Name) || string.IsNullOrEmpty(Surname) || string.IsNullOrEmpty(EmployDate));
        }

        public override string ToString()
        {
            return Name + " " + Surname;
        }
    }
}
