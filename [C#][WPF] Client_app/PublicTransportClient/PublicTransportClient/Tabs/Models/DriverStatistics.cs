﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using Newtonsoft.Json;

namespace PublicTransportClient.Tabs.Models
{
    public class DriverStatistics : ModelBase
    {
        private String _DriverStr;

        [JsonProperty(PropertyName = "driver_str")]
        public String DriverStr
        {
            get { return _DriverStr; }
            set
            {
                _DriverStr = value;
                RaisePropertyChanged(() => DriverStr);
            }
        }

        private String _DayType;

        [JsonProperty(PropertyName = "day_type")]
        public String DayType
        {
            get { return _DayType; }
            set
            {
                _DayType = value;
                RaisePropertyChanged(() => DayType);
            }
        }

        private int _RidesCount;
        
        [JsonProperty(PropertyName = "rides_count")]
        public int RidesCount
        {
            get { return _RidesCount; }
            set
            {
                _RidesCount = value;
                RaisePropertyChanged(() => RidesCount);
            }
        }


        public override bool IsValid()
        {
            throw new NotImplementedException();
        }
    }
}
