﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace PublicTransportClient.Tabs.Models
{
    public class RestResponseDBError : ObservableObject
    {
        private String _Query;

        public String Query
        {
            get { return _Query; }
            set
            {
                _Query = value;
                RaisePropertyChanged(() => Query);
            }
        }
        

        private String _Error;

        // not required: [JsonProperty(PropertyName = "error")]
        public String Error
        {
            get { return _Error; }
            set
            {
                _Error = value;
                RaisePropertyChanged(() => Error);
            }
        }
        
    }
}
