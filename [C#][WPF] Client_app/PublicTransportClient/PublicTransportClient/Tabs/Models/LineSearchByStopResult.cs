﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublicTransportClient.Tabs.Models
{
    public class LineSearchByStopResult : ModelBase
    {
        private int _LineId;

        public int LineId
        {
            get { return _LineId; }
            set
            {
                _LineId = value;
                RaisePropertyChanged(() => LineId);
            }
        }
        

        private String _FromTime;

        public String FromTime
        {
            get { return _FromTime; }
            set
            {
                _FromTime = value;
                RaisePropertyChanged(() => FromTime);
            }
        }

        private String _ToTime;

        public String ToTime
        {
            get { return _ToTime; }
            set
            {
                _ToTime = value;
                RaisePropertyChanged(() => ToTime);
            }
        }

        public override bool IsValid()
        {
            throw new NotImplementedException();
        }
    }
}
