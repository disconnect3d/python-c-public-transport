﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using Newtonsoft.Json;

namespace PublicTransportClient.Tabs.Models
{
    public abstract class ModelBase : ObservableObject
    {
        private int _Id;

        [JsonProperty(PropertyName = "id")]
        public int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
                RaisePropertyChanged(() => Id);
            }
        }

        public abstract bool IsValid();


        #region ADDED FOR EASIER ERROR REPORTING // may be not the best way -- TODO: Refactor this?
        private String _Query;

        public String Query
        {
            get { return _Query; }
            set
            {
                _Query = value;
                RaisePropertyChanged(() => Query);
            }
        }
        private String _Error;

        public String Error
        {
            get { return _Error; }
            set
            {
                _Error = value;
                RaisePropertyChanged(() => Error);
            }
        }
        #endregion
    }
}
