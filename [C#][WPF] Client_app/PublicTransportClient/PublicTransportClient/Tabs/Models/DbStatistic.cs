﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace PublicTransportClient.Tabs.Models
{
    public class DbStatistic : ObservableObject
    {
        private String _TableName;

        public String TableName
        {
            get { return _TableName; }
            set
            {
                _TableName = value;
                RaisePropertyChanged(() => TableName);
            }
        }

        private int _RecordsCount;

        public int RecordsCount
        {
            get { return _RecordsCount; }
            set
            {
                _RecordsCount = value;
                RaisePropertyChanged(() => RecordsCount);
            }
        }
        
        
    }
}
