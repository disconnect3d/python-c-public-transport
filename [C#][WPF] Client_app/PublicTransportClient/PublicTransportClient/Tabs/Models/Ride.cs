﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Newtonsoft.Json;

namespace PublicTransportClient.Tabs.Models
{
    public class Ride : ModelBase
    {
        private int _LineId;
        
        [JsonProperty(PropertyName = "line_id")]
        public int LineId
        {
            get { return _LineId; }
            set
            {
                _LineId = value;
                RaisePropertyChanged(() => LineId);
            }
        }
        

        private int _DayTypeId;

        [JsonProperty(PropertyName = "day_type_id")]
        public int DayTypeId
        {
            get { return _DayTypeId; }
            set
            {
                _DayTypeId = value;
                RaisePropertyChanged(() => DayTypeId);
            }
        }

        private int _VehicleId;

        [JsonProperty(PropertyName = "vehicle_id")]
        public int VehicleId
        {
            get { return _VehicleId; }
            set
            {
                _VehicleId = value;
                RaisePropertyChanged(() => VehicleId);
            }
        }

        private int _DriverId;

        [JsonProperty(PropertyName = "driver_id")]
        public int DriverId
        {
            get { return _DriverId; }
            set
            {
                _DriverId = value;
                RaisePropertyChanged(() => DriverId);
            }
        }

        private int _RideNumber;

        [JsonProperty(PropertyName = "ride_number")]
        public int RideNumber
        {
            get { return _RideNumber; }
            set
            {
                _RideNumber = value;
                RaisePropertyChanged(() => RideNumber);
            }
        }

        private string _Direction;

        [JsonProperty(PropertyName = "direction")]
        public string Direction
        {
            get { return _Direction; }
            set
            {
                _Direction = value;
                RaisePropertyChanged(() => Direction);
            }
        }
        
        
        public override bool IsValid()
        {
            return true;
        }

        public override string ToString()
        {
            string dir = Direction == "True"? "A -> Z" : "Z -> A";
            return "Id: " + Id + ", Linia: " + LineId + ", nr przejazdu: " + RideNumber + ", kierunek: " + dir;
        }
    }
}
