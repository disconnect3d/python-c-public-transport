﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace PublicTransportClient.Tabs.Models
{
    public class DayType : ModelBase
    {
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set
            {
                _Name = value;
                RaisePropertyChanged(() => Name);
            }
        }

        public override bool IsValid()
        {
            return !string.IsNullOrEmpty(Name);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
