﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using PublicTransportClient.Tabs.Models;

namespace PublicTransportClient.Tabs.Converters
{
    public class IdToRideStrConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ObservableCollection<Ride> coll = values[1] as ObservableCollection<Ride>;

            int rideId = (int)values[0];

            return coll.First((r) => r.Id == rideId).ToString();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
