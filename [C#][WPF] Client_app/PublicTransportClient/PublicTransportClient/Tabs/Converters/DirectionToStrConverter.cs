﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using PublicTransportClient.Tabs.Models;

namespace PublicTransportClient.Tabs.Converters
{
    public class DirectionToStrConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ObservableCollection<string> coll = (ObservableCollection<string>)values[1];

            bool direction = (string)values[0] == "True";

            return direction ? coll[0] : coll[1];
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
