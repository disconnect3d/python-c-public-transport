﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using PublicTransportClient.Tabs.Models;

namespace PublicTransportClient.Tabs.Converters
{
    public class IdToDriverStrConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ObservableCollection<Driver> coll = values[1] as ObservableCollection<Driver>;

            int driverId = (int)values[0];

            return coll.First((d) => d.Id == driverId).ToString();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
