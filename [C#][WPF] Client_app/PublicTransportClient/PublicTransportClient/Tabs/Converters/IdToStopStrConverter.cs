﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using PublicTransportClient.Tabs.Models;

namespace PublicTransportClient.Tabs.Converters
{
    public class IdToStopStrConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ObservableCollection<Stop> coll = values[1] as ObservableCollection<Stop>;

            int stopId = (int)values[0];

            return coll.First((s) => s.Id == stopId).ToString();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
