﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using PublicTransportClient.Tabs.Models;

namespace PublicTransportClient.Tabs.Converters
{
    public class IdToVehicleStrConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ObservableCollection<Vehicle> coll = values[1] as ObservableCollection<Vehicle>;

            int vehicleId = (int) values[0];

            return coll.First((v) => v.Id == vehicleId).ToString();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
