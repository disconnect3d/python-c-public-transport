﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using GalaSoft.MvvmLight.Ioc;
using PublicTransportClient.Tabs.Models;
using PublicTransportClient.Tabs.ViewModels;

namespace PublicTransportClient.Tabs.Converters
{
    public class IdToStreetNameConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ObservableCollection<Street> coll = values[1] as ObservableCollection<Street>;

            int streetId = (int) values[0];

            return coll.First((v) => v.Id == streetId).StreetName;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
