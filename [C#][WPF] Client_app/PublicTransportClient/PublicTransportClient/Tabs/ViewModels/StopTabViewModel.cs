﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using PublicTransportClient.Tabs.Models;
using PublicTransportClient.ViewModel;
using RestSharp;
using System.Linq;

namespace PublicTransportClient.Tabs.ViewModels
{
    public class StopTabViewModel : TabViewModelBase<Stop>
    {
        public StopTabViewModel(RestClient rc)
            : base(rc, Properties.Settings.Default.ApiPostfix_Stop)
        {
            if (!IsInDesignMode)
            {
                GetData();
                AddNewStopCommand = new RelayCommand(AddNewStop);
            }
        }

        public override void GetData()
        {
            base.GetData();

            Streets = GetCollection<Street>(Properties.Settings.Default.ApiPostfix_Street);
        }

        private ObservableCollection<Street> _Streets;

        public ObservableCollection<Street> Streets
        {
            get { return _Streets; }
            set
            {
                _Streets = value;
                RaisePropertyChanged(() => Streets);
            }
        }

        private Street _NewStopStreet;

        public Street NewStopStreet
        {
            get { return _NewStopStreet; }
            set
            {
                _NewStopStreet = value;
                RaisePropertyChanged(() => NewStopStreet);
                ValidateStopEnabled();
            }
        }

        private string _NewStopName;

        public string NewStopName
        {
            get { return _NewStopName; }
            set
            {
                _NewStopName = value;
                RaisePropertyChanged(() => NewStopName);
                ValidateStopEnabled();
            }
        }

        private string _NewStopId;

        public string NewStopId
        {
            get { return _NewStopId; }
            set
            {
                _NewStopId = value;
                RaisePropertyChanged(() => NewStopId);
                ValidateStopEnabled();
            }
        }

        private void ValidateStopEnabled()
        {
            if (NewStopStreet == null || string.IsNullOrEmpty(NewStopName))
            {
                AddNewStopEnabled = false;
                return;
            }

            try
            {
                int.Parse(NewStopId);
            }
            catch
            {
                AddNewStopEnabled = false;
                return;
            }

            AddNewStopEnabled = true;
        }

        private bool _AddNewStopEnabled;

        public bool AddNewStopEnabled
        {
            get { return _AddNewStopEnabled; }
            set
            {
                _AddNewStopEnabled = value;
                RaisePropertyChanged(() => AddNewStopEnabled);
            }
        }
        

        public ICommand AddNewStopCommand { get; private set; }

        private void AddNewStop()
        {
            int stopId = int.Parse(NewStopId);

            Stop newStop = new Stop()
            {
                Id = stopId,
                StreetId = NewStopStreet.Id,
                StopName = NewStopName
            };

            if (Streets.Any((s) => s.Id == stopId))
                PutObject(newStop);
            else
                PostObject(newStop);

            GetData();
        }
        
        
    }
}