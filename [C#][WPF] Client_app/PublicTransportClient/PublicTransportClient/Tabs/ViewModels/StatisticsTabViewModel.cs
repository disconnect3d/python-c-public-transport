﻿using System.Collections.ObjectModel;
using PublicTransportClient.Tabs.Models;
using PublicTransportClient.ViewModel;
using RestSharp;

namespace PublicTransportClient.Tabs.ViewModels
{
    public class StatisticsTabViewModel : RCViewModelBase
    {
        public StatisticsTabViewModel(RestClient rc)
            : base(rc)
        {}

        public override void GetData()
        {
            DriverStats = GetCollection<DriverStatistics>(Properties.Settings.Default.ApiPostfix_DriverStats);
        }

        private ObservableCollection<DriverStatistics> _DriverStats;

        public ObservableCollection<DriverStatistics> DriverStats
        {
            get { return _DriverStats; }
            set
            {
                _DriverStats = value;
                RaisePropertyChanged(() => DriverStats);
            }
        }
        
    }
}