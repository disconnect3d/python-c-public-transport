﻿using PublicTransportClient.Tabs.Models;
using PublicTransportClient.ViewModel;
using RestSharp;

namespace PublicTransportClient.Tabs.ViewModels
{
    public class DriverTabViewModel : TabViewModelBase<Driver>
    {
        public DriverTabViewModel(RestClient rc)
            : base(rc, Properties.Settings.Default.ApiPostfix_Driver)
        {}
    }
}