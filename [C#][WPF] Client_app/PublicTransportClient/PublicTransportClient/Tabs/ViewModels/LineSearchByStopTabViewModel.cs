﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using PublicTransportClient.Tabs.Models;
using PublicTransportClient.ViewModel;
using RestSharp;
using System.Linq;
using GalaSoft.MvvmLight.Command;

namespace PublicTransportClient.Tabs.ViewModels
{
    public class LineSearchByStopTabViewModel : RCViewModelBase
    {
        public LineSearchByStopTabViewModel(RestClient rc)
            : base(rc)
        {
            if (!IsInDesignMode)
                SearchLineByStopsCommand = new RelayCommand(Search);
        }

        public override void GetData()
        {
            Stops = GetCollection<Stop>(Properties.Settings.Default.ApiPostfix_Stop);
            DayTypes = GetCollection<DayType>(Properties.Settings.Default.ApiPostfix_DayType);
        }


        private void Search()
        {
            StopFrom_Name_Searched = StopFrom.StopName;
            StopTo_Name_Searched = StopTo.StopName;
            DayType_Name_Searched = SelectedDayType.Name;

            var req = new RestRequest("api/search_line/{stop_id_from}/{stop_id_to}/{day_type_id}", Method.GET);
            req.AddUrlSegment("stop_id_from", StopFrom.Id.ToString());
            req.AddUrlSegment("stop_id_to", StopTo.Id.ToString());
            req.AddUrlSegment("day_type_id", SelectedDayType.Id.ToString());

            var resp = RC.Execute<ObservableCollection<LineSearchByStopResult>>(req);
            SearchResults = resp.Data;
        }


        private ObservableCollection<LineSearchByStopResult> _SearchResults;

        public ObservableCollection<LineSearchByStopResult> SearchResults
        {
            get { return _SearchResults; }
            set
            {
                _SearchResults = value;
                RaisePropertyChanged(() => SearchResults);
            }
        }
        


        private Stop _StopFrom;

        public Stop StopFrom
        {
            get { return _StopFrom; }
            set
            {
                _StopFrom = value;
                RaisePropertyChanged(() => StopFrom);
                RefreshIsSearchEnabled();
            }
        }

        private Stop _StopTo;

        public Stop StopTo
        {
            get { return _StopTo; }
            set
            {
                _StopTo = value;
                RaisePropertyChanged(() => StopTo);
                RefreshIsSearchEnabled();
            }
        }
        
        

        private ObservableCollection<Stop> _Stops;

        public ObservableCollection<Stop> Stops
        {
            get { return _Stops; }
            set
            {
                _Stops = value;
                RaisePropertyChanged(() => Stops);
            }
        }

        private ObservableCollection<DayType> _DayTypes;

        public ObservableCollection<DayType> DayTypes
        {
            get { return _DayTypes; }
            set
            {
                _DayTypes = value;
                RaisePropertyChanged(() => DayTypes);
            }
        }

        private DayType _SelectedDayType;

        public DayType SelectedDayType
        {
            get { return _SelectedDayType; }
            set
            {
                _SelectedDayType = value;
                RaisePropertyChanged(() => SelectedDayType);
                RefreshIsSearchEnabled();
            }
        }
        

        public ICommand SearchLineByStopsCommand { get; private set; }

        private String _StopFrom_Name_Searched;

        public String StopFrom_Name_Searched
        {
            get { return _StopFrom_Name_Searched; }
            set
            {
                _StopFrom_Name_Searched = value;
                RaisePropertyChanged(() => StopFrom_Name_Searched);
            }
        }

        private String _StopTo_Name_Searched;

        public String StopTo_Name_Searched
        {
            get { return _StopTo_Name_Searched; }
            set
            {
                _StopTo_Name_Searched = value;
                RaisePropertyChanged(() => StopTo_Name_Searched);
            }
        }

        private String _DayType_Name_Searched;

        public String DayType_Name_Searched
        {
            get { return _DayType_Name_Searched; }
            set
            {
                _DayType_Name_Searched = value;
                RaisePropertyChanged(() => DayType_Name_Searched);
            }
        }

        private bool _IsSearchEnabled;

        public bool IsSearchEnabled
        {
            get { return _IsSearchEnabled; }
            set
            {
                _IsSearchEnabled = value;
                RaisePropertyChanged(() => IsSearchEnabled);
            }
        }

        private void RefreshIsSearchEnabled()
        {
            if (StopFrom == null || StopTo == null || SelectedDayType == null)
                IsSearchEnabled = false;
            else
                IsSearchEnabled = true;
        }
        
        
    }
}