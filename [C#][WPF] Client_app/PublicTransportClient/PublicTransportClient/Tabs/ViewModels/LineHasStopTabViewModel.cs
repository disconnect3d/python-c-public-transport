﻿using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using PublicTransportClient.Tabs.Models;
using PublicTransportClient.ViewModel;
using RestSharp;

namespace PublicTransportClient.Tabs.ViewModels
{
    public class LineHasStopTabViewModel : TabViewModelBase<LineHasStop>
    {
        public LineHasStopTabViewModel(RestClient rc)
            : base(rc, Properties.Settings.Default.ApiPostfix_LineHasStop)
        {
            if (!IsInDesignMode)
            {
                AddNewLineHasStopCommand = new RelayCommand(AddData);
            }
        }

        public override void GetData()
        {
            var settings = Properties.Settings.Default;
            Objects = GetCollection<LineHasStop>(ApiPostfixUrl);
            Stops = GetCollection<Stop>(settings.ApiPostfix_Stop);
            Lines = GetCollection<Line>(settings.ApiPostfix_Line);
        }

        protected override void DeleteData(KeyEventArgs args)
        {
            if (args.Key != Key.Delete || (args.OriginalSource as DataGridCell) == null)
                return;

            var request = new RestRequest(ApiPostfixUrl + "/{line_id}/{stop_id}", Method.DELETE);
            request.AddUrlSegment("line_id", SelectedObject.LineId.ToString());
            request.AddUrlSegment("stop_id", SelectedObject.StopId.ToString());
            request.RequestFormat = DataFormat.Json;

            RC.Execute(request);

            GetData();
        }

        private void AddData()
        {
            int stopOrder = int.Parse(NewLineHasStopStopOrder);
            LineHasStop newLineHasStop = new LineHasStop()
            {
                LineId = NewLineHasStopLine.Id,
                StopId = NewLineHasStopStop.Id,
                StopOrder = stopOrder
            };

            PostObject(newLineHasStop);

            GetData();
        }

        private void ValidateNewnewLineHasStopEnabled()
        {
            if (NewLineHasStopLine == null || NewLineHasStopStop == null)
            {
                AddNewLineHasStopEnabled = false;
                return;
            }

            try
            {
                int.Parse(NewLineHasStopStopOrder);
            }
            catch
            {
                AddNewLineHasStopEnabled = false;
                return;
            }

            AddNewLineHasStopEnabled = true;
        }

        #region Properties needed to ADD LineHasStop
        private ObservableCollection<Line> _Lines;

        public ObservableCollection<Line> Lines
        {
            get { return _Lines; }
            set
            {
                _Lines = value;
                RaisePropertyChanged(() => Lines);
            }
        }

        private ObservableCollection<Stop> _Stops;

        public ObservableCollection<Stop> Stops
        {
            get { return _Stops; }
            set
            {
                _Stops = value;
                RaisePropertyChanged(() => Stops);
            }
        }

        private string _NewLineHasStopStopOrder;

        public string NewLineHasStopStopOrder
        {
            get { return _NewLineHasStopStopOrder; }
            set
            {
                _NewLineHasStopStopOrder = value;
                RaisePropertyChanged(() => NewLineHasStopStopOrder);
                ValidateNewnewLineHasStopEnabled();
            }
        }

        private Line _NewLineHasStopLine;

        public Line NewLineHasStopLine
        {
            get { return _NewLineHasStopLine; }
            set
            {
                _NewLineHasStopLine = value;
                RaisePropertyChanged(() => NewLineHasStopLine);
                ValidateNewnewLineHasStopEnabled();
            }
        }

        private Stop _NewLineHasStopStop;

        public Stop NewLineHasStopStop
        {
            get { return _NewLineHasStopStop; }
            set
            {
                _NewLineHasStopStop = value;
                RaisePropertyChanged(() => NewLineHasStopStop);
                ValidateNewnewLineHasStopEnabled();
            }
        }
        
        

        private bool _AddNewLineHasStopEnabled;

        public bool AddNewLineHasStopEnabled
        {
            get { return _AddNewLineHasStopEnabled; }
            set
            {
                _AddNewLineHasStopEnabled = value;
                RaisePropertyChanged(() => AddNewLineHasStopEnabled);
            }
        }

        public ICommand AddNewLineHasStopCommand { get; private set; }
        #endregion
    }
}
