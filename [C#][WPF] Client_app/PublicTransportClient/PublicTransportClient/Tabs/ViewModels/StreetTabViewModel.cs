﻿using PublicTransportClient.Tabs.Models;
using PublicTransportClient.ViewModel;
using RestSharp;

namespace PublicTransportClient.Tabs.ViewModels
{
    public class StreetTabViewModel : TabViewModelBase<Street>
    {
        public StreetTabViewModel(RestClient rc)
            : base(rc, Properties.Settings.Default.ApiPostfix_Street)
        {
            if (!IsInDesignMode)
                GetData();
        }
    }
}