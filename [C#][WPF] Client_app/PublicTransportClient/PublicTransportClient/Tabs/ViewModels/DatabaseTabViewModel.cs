﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using PublicTransportClient.Tabs.Models;
using PublicTransportClient.ViewModel;
using RestSharp;

namespace PublicTransportClient.Tabs.ViewModels
{
    public class DatabaseTabViewModel : RCViewModelBase
    {
        public DatabaseTabViewModel(RestClient rc)
            : base(rc)
        {
            _APIAddress = Properties.Settings.Default.ApiAddress;
        }

        private ObservableCollection<DbStatistic> _DbStatistics;

        public ObservableCollection<DbStatistic> DbStatistics
        {
            get { return _DbStatistics; }
            set
            {
                _DbStatistics = value;
                RaisePropertyChanged(() => DbStatistics);
            }
        }
        

        public override void GetData()
        {
            var request = new RestRequest("db/stats", Method.GET);
            var response = RC.Execute<ObservableCollection<DbStatistic>>(request);

            DbStatistics = response.Data;
        }

        private string _APIAddress;

        public string APIAddress
        {
            get { return _APIAddress; }
            set
            {
                _APIAddress = value;
                Properties.Settings.Default.ApiAddress = value;
                Properties.Settings.Default.Save();
                RaisePropertyChanged(() => APIAddress);
            }
        }

        public ICommand APIAddressToDefaultCommand {
            get
            {
                return new RelayCommand(() => { APIAddress = "http://disconnect3d.pl:5000/"; });
            }
        }

        public ICommand ResetDBCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    var req = new RestRequest("db/reset", Method.PUT);
                    req.ReadWriteTimeout = 5000;
                    RC.Execute(req);

                    GetData();
                });
            }
        }
    }
}
