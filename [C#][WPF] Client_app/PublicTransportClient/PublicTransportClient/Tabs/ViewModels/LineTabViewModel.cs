﻿using PublicTransportClient.Tabs.Models;
using PublicTransportClient.ViewModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using PublicTransportClient.Common;

namespace PublicTransportClient.Tabs.ViewModels
{
    public class LineTabViewModel : RCViewModelBase
    {
        public LineTabViewModel(RestClient rc)
            : base(rc)
        {
            Directions = new ObservableCollection<Tuple<string, bool>>();
            Directions.Add(new Tuple<string, bool>("A -> Z", true));
            Directions.Add(new Tuple<string, bool>("Z -> A", false));
            SelectedDirection = Directions[0];
            NewLineCommand = new RelayCommand(NewLine);
        }

        public override void GetData()
        {
            GetLines();
            GetDayTypes();
            GetScheduleForLine();
        }

        private void GetLines()
        {
            var request = new RestRequest("api/line", Method.GET);
            var response = RC.Execute<ObservableCollection<Line>>(request);
            Lines = response.Data;

            if (Lines != null && Lines.Count > 0)
                SelectedLine = Lines[0];
        }

        private void GetDayTypes()
        {
            var request = new RestRequest("api/day_type", Method.GET);
            var response = RC.Execute<ObservableCollection<DayType>>(request);

            DayTypes = response.Data;

            if (DayTypes != null && DayTypes.Count > 0)
                SelectedDayType = DayTypes[0];
        }

        private void GetScheduleForLine()
        {
            if (SelectedLine == null || SelectedDayType == null || _SelectedDirection == null)
                return;

            var request = new RestRequest("api/schedule_for_line/{line_id}/{day_type_id}/{direction}", Method.GET);
            request.AddUrlSegment("line_id", SelectedLine.Id.ToString());
            request.AddUrlSegment("day_type_id", SelectedDayType.Id.ToString());
            request.AddUrlSegment("direction", SelectedDirection.Item2? "1" : "0");

            var response = RC.Execute<ObservableCollection<ScheduleForLine>>(request);
            SchedulesForLine = response.Data;
        }

        private string _NewLineId;

        public string NewLineId
        {
            get { return _NewLineId; }
            set
            {
                _NewLineId = value;
                RaisePropertyChanged(() => NewLineId);
            }
        }

        public ICommand NewLineCommand { get; private set; }
        
        public void NewLine()
        {
            Line obj = new Line() { Id = int.Parse(NewLineId) };

            var postRequest = new RestRequest("api/line", Method.POST);
            postRequest.JsonizeObject(obj);

            var resp = RC.Execute<RestResponseDBError>(postRequest);

            if (resp.Data != null && resp.Data.Error != null)
                Messenger.Default.Send(resp.Data);

        }
        

        private ObservableCollection<Line> _Lines;
        public ObservableCollection<Line> Lines
        {
            get { return _Lines; }
            set
            {
                _Lines = value;
                RaisePropertyChanged(() => Lines);
            }
        }


        private ObservableCollection<Stop> _Stops;
        public ObservableCollection<Stop> Stops
        {
            get { return _Stops; }
            set
            {
                _Stops = value;
                RaisePropertyChanged(() => Stops);
            }
        }

        private ObservableCollection<DayType> _DayTypes;

        public ObservableCollection<DayType> DayTypes
        {
            get { return _DayTypes; }
            set
            {
                _DayTypes = value;
                RaisePropertyChanged(() => DayTypes);
            }
        }

        private Line _SelectedLine;
        public Line SelectedLine
        {
            get { return _SelectedLine; }
            set
            {
                _SelectedLine = value;
                RaisePropertyChanged(() => SelectedLine);
                GetScheduleForLine();
            }
        }

        private DayType _SelectedDayType;

        public DayType SelectedDayType
        {
            get { return _SelectedDayType; }
            set
            {
                _SelectedDayType = value;
                RaisePropertyChanged(() => SelectedDayType); ;
                GetScheduleForLine();
            }
        }

        public ObservableCollection<Tuple<String, Boolean>> Directions { get; set; }

        private Tuple<String, Boolean> _SelectedDirection;

        public Tuple<String, Boolean> SelectedDirection
        {
            get { return _SelectedDirection; }
            set
            {
                _SelectedDirection = value;
                RaisePropertyChanged(() => SelectedDirection); ;
                GetScheduleForLine();
            }
        }

        private ObservableCollection<ScheduleForLine> _SchedulesForLine;

        public ObservableCollection<ScheduleForLine> SchedulesForLine
        {
            get { return _SchedulesForLine; }
            set
            {
                _SchedulesForLine = value;
                RaisePropertyChanged(() => SchedulesForLine);
            }
        }
    }
}
