﻿using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using PublicTransportClient.Tabs.Models;
using PublicTransportClient.ViewModel;
using RestSharp;

namespace PublicTransportClient.Tabs.ViewModels
{
    public class ScheduleTabViewModel : TabViewModelBase<Schedule>
    {
        public ScheduleTabViewModel(RestClient rc)
            : base(rc, Properties.Settings.Default.ApiPostfix_Schedule)
        {
            if (!IsInDesignMode)
            {
                AddNewScheduleCommand = new RelayCommand(AddData);
            }
        }

        public override void GetData()
        {
            var settings = Properties.Settings.Default;
            Objects = GetCollection<Schedule>(ApiPostfixUrl);
            Stops = GetCollection<Stop>(settings.ApiPostfix_Stop);
            Rides = GetCollection<Ride>(settings.ApiPostfix_Ride);
        }


        private void AddData()
        {
            int scheduleId = int.Parse(NewScheduleScheduleId);
            int scheduleStopCounter = int.Parse(NewScheduleStopCounter);

            Schedule newSchedule = new Schedule()
            {
                Id = scheduleId,
                RideId = NewScheduleRide.Id,
                StopId = NewScheduleStop.Id,
                DepartureTime = NewScheduleDepartureTime,
                StopCounter = scheduleStopCounter
            };

            if (Objects.Any((s) => s.Id == scheduleId))
                PutObject(newSchedule);
            else
                PostObject(newSchedule);

            GetData();
        }

        private void ValidateNewScheduleEnabled()
        {
            if (NewScheduleRide == null || NewScheduleStop == null)
            {
                AddNewScheduleEnabled = false;
                return;
            }

            try
            {
                int.Parse(NewScheduleScheduleId);
                int.Parse(NewScheduleStopCounter);
                DateTime.ParseExact(NewScheduleDepartureTime, "hh:mm:ss", CultureInfo.InvariantCulture);
            }
            catch
            {
                AddNewScheduleEnabled = false;
                return;
            }

            AddNewScheduleEnabled = true;
        }

        #region Properties needed to ADD/MODIFY Schedule
        private ObservableCollection<Ride> _Rides;

        public ObservableCollection<Ride> Rides
        {
            get { return _Rides; }
            set
            {
                _Rides = value;
                RaisePropertyChanged(() => Rides);
            }
        }

        private ObservableCollection<Stop> _Stops;

        public ObservableCollection<Stop> Stops
        {
            get { return _Stops; }
            set
            {
                _Stops = value;
                RaisePropertyChanged(() => Stops);
            }
        }
        
        

        private String _NewScheduleScheduleId;

        public String NewScheduleScheduleId
        {
            get { return _NewScheduleScheduleId; }
            set
            {
                _NewScheduleScheduleId = value;
                RaisePropertyChanged(() => NewScheduleScheduleId);
                ValidateNewScheduleEnabled();
            }
        }

        private Ride _NewScheduleRide;

        public Ride NewScheduleRide
        {
            get { return _NewScheduleRide; }
            set
            {
                _NewScheduleRide = value;
                RaisePropertyChanged(() => NewScheduleRide);
                ValidateNewScheduleEnabled();
            }
        }

        private Stop _NewScheduleStop;

        public Stop NewScheduleStop
        {
            get { return _NewScheduleStop; }
            set
            {
                _NewScheduleStop = value;
                RaisePropertyChanged(() => NewScheduleStop);
                ValidateNewScheduleEnabled();
            }
        }

        private String _NewScheduleDepartureTime;

        public String NewScheduleDepartureTime
        {
            get { return _NewScheduleDepartureTime; }
            set
            {
                _NewScheduleDepartureTime = value;
                RaisePropertyChanged(() => NewScheduleDepartureTime);
                ValidateNewScheduleEnabled();
            }
        }

        private String _NewScheduleStopCounter;

        public String NewScheduleStopCounter
        {
            get { return _NewScheduleStopCounter; }
            set
            {
                _NewScheduleStopCounter = value;
                RaisePropertyChanged(() => NewScheduleStopCounter);
                ValidateNewScheduleEnabled();
            }
        }

        private bool _AddNewScheduleEnabled;

        public bool AddNewScheduleEnabled
        {
            get { return _AddNewScheduleEnabled; }
            set
            {
                _AddNewScheduleEnabled = value;
                RaisePropertyChanged(() => AddNewScheduleEnabled);
            }
        }

        public ICommand AddNewScheduleCommand { get; private set; }
        #endregion
    }
}
