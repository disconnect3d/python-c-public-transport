﻿using PublicTransportClient.ViewModel;
using RestSharp;
using PublicTransportClient.Tabs.Models;

namespace PublicTransportClient.Tabs.ViewModels
{
    public class VehicleTabViewModel : TabViewModelBase<Vehicle>
    {
        public VehicleTabViewModel(RestClient rc)
            : base(rc, Properties.Settings.Default.ApiPostfix_Vehicle)
        {}        
    }
}
