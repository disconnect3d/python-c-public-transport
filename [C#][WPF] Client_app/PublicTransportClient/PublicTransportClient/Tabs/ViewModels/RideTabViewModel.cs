﻿using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using PublicTransportClient.Tabs.Models;
using PublicTransportClient.ViewModel;
using RestSharp;

namespace PublicTransportClient.Tabs.ViewModels
{
    public class RideTabViewModel : TabViewModelBase<Ride>
    {
        public RideTabViewModel(RestClient rc)
            : base(rc, Properties.Settings.Default.ApiPostfix_Ride)
        {
            if (!IsInDesignMode)
            {
                Directions = new ObservableCollection<string>();
                Directions.Add("A -> Z");
                Directions.Add("Z -> A");

                AddNewStopCommand = new RelayCommand(AddData);
            }
        }

        public override void GetData()
        {
            var settings = Properties.Settings.Default;
            Lines = GetCollection<Line>(settings.ApiPostfix_Line);
            Objects = GetCollection<Ride>(settings.ApiPostfix_Ride);
            DayTypes = GetCollection<DayType>(settings.ApiPostfix_DayType);
            Vehicles = GetCollection<Vehicle>(settings.ApiPostfix_Vehicle);
            Drivers = GetCollection<Driver>(settings.ApiPostfix_Driver);
        }

        public ICommand AddNewStopCommand { get; private set; }

        private void AddData()
        {
            int rideId = int.Parse(NewRideRideId);
            int rideNumber = int.Parse(NewRideRideNumber);

            Ride newRide = new Ride()
            {
                Id = rideId,
                DayTypeId = NewRideDayType.Id,
                VehicleId = NewRideVehicle.Id,
                DriverId = NewRideDriver.Id,
                LineId = NewRideLine.Id,
                Direction = NewRideDirection == Directions[0] ? "True" : "",
                RideNumber = rideNumber

            };

            if (Objects.Any((r) => r.Id == rideId))
                PutObject(newRide);
            else
                PostObject(newRide);

            GetData();
        }

        private void ValidateNewRideEnabled()
        {
            if (NewRideDayType == null || NewRideVehicle == null || NewRideDriver == null || NewRideLine == null)
            {
                AddNewRideEnabled = false;
                return;
            }

            try
            {
                int.Parse(NewRideRideId);
                int.Parse(NewRideRideNumber);
            }
            catch
            {
                AddNewRideEnabled = false;
                return;
            }

            AddNewRideEnabled = true;
        }

        private String _LineId;

        public String LineId
        {
            get { return _LineId; }
            set
            {
                _LineId = value;
                RaisePropertyChanged(() => LineId);
            }
        }

        private ObservableCollection<Line> _Lines;

        public ObservableCollection<Line> Lines
        {
            get { return _Lines; }
            set
            {
                _Lines = value;
                RaisePropertyChanged(() => Lines);
            }
        }
        

        private ObservableCollection<DayType> _DayTypes;

        public ObservableCollection<DayType> DayTypes
        {
            get { return _DayTypes; }
            set
            {
                _DayTypes = value;
                RaisePropertyChanged(() => DayTypes);
            }
        }

        private ObservableCollection<Vehicle> _Vehicles;

        public ObservableCollection<Vehicle> Vehicles
        {
            get { return _Vehicles; }
            set
            {
                _Vehicles = value;
                RaisePropertyChanged(() => Vehicles);
            }
        }

        private ObservableCollection<Driver> _Drivers;

        public ObservableCollection<Driver> Drivers
        {
            get { return _Drivers; }
            set
            {
                _Drivers = value;
                RaisePropertyChanged(() => Drivers);
            }
        }

        private ObservableCollection<String> _Directions;

        public ObservableCollection<String> Directions
        {
            get { return _Directions; }
            set
            {
                _Directions = value;
                RaisePropertyChanged(() => Directions);
            }
        }

        private String _NewRideRideId;

        public String NewRideRideId
        {
            get { return _NewRideRideId; }
            set
            {
                _NewRideRideId = value;
                RaisePropertyChanged(() => NewRideRideId);
                ValidateNewRideEnabled();
            }
        }

        private Line _NewRideLine;

        public Line NewRideLine
        {
            get { return _NewRideLine; }
            set
            {
                _NewRideLine = value;
                RaisePropertyChanged(() => NewRideLine);
                ValidateNewRideEnabled();
            }
        }

        private DayType _NewRideDayType;

        public DayType NewRideDayType
        {
            get { return _NewRideDayType; }
            set
            {
                _NewRideDayType = value;
                RaisePropertyChanged(() => NewRideDayType);
                ValidateNewRideEnabled();
            }
        }

        private String _NewRideRideNumber;

        public String NewRideRideNumber
        {
            get { return _NewRideRideNumber; }
            set
            {
                _NewRideRideNumber = value;
                RaisePropertyChanged(() => NewRideRideNumber);
                ValidateNewRideEnabled();
            }
        }

        private Driver _NewRideDriver;

        public Driver NewRideDriver
        {
            get { return _NewRideDriver; }
            set
            {
                _NewRideDriver = value;
                RaisePropertyChanged(() => NewRideDriver);
                ValidateNewRideEnabled();
            }
        }

        private Vehicle _NewRideVehicle;

        public Vehicle NewRideVehicle
        {
            get { return _NewRideVehicle; }
            set
            {
                _NewRideVehicle = value;
                RaisePropertyChanged(() => NewRideVehicle);
                ValidateNewRideEnabled();
            }
        }

        private string _NewRideDirection;

        public string NewRideDirection
        {
            get { return _NewRideDirection; }
            set
            {
                _NewRideDirection = value;
                RaisePropertyChanged(() => NewRideDirection);
                ValidateNewRideEnabled();
            }
        }

        private bool _AddNewRideEnabled;

        public bool AddNewRideEnabled
        {
            get { return _AddNewRideEnabled; }
            set
            {
                _AddNewRideEnabled = value;
                RaisePropertyChanged(() => AddNewRideEnabled);
            }
        }
    }
}
