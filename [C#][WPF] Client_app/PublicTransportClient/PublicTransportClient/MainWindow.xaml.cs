﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GalaSoft.MvvmLight.Messaging;
using PublicTransportClient.Tabs.Models;
using PublicTransportClient.Tabs.Views;

namespace PublicTransportClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Messenger.Default.Register<RestResponseDBError>(this, ShowErrorDialog);
        }

        private void ShowErrorDialog(RestResponseDBError err)
        {
            String msg = err.Error;

            if (!String.IsNullOrEmpty(err.Query))
                msg = "Błąd podczas wykonywania zapytania SQL:\n" + err.Query + "\n\n" + msg;
            MessageBox.Show(msg, "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
