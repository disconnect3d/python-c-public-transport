/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:PublicTransportClient"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using PublicTransportClient.Tabs.ViewModels;
using RestSharp;

namespace PublicTransportClient.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            ////if (ViewModelBase.IsInDesignModeStatic)
            ////{
            ////    // Create design time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DesignDataService>();
            ////}
            ////else
            ////{
            ////    // Create run time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DataService>();
            ////}

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<StreetTabViewModel>();
            SimpleIoc.Default.Register<StopTabViewModel>();
            SimpleIoc.Default.Register<DriverTabViewModel>();
            SimpleIoc.Default.Register<LineTabViewModel>();
            SimpleIoc.Default.Register<VehicleTabViewModel>();
            SimpleIoc.Default.Register<DatabaseTabViewModel>();
            SimpleIoc.Default.Register<RideTabViewModel>();
            SimpleIoc.Default.Register<ScheduleTabViewModel>();
            SimpleIoc.Default.Register<LineHasStopTabViewModel>();
            SimpleIoc.Default.Register<StatisticsTabViewModel>();
            SimpleIoc.Default.Register<LineSearchByStopTabViewModel>();
            SimpleIoc.Default.Register(() => new RestClient(Properties.Settings.Default.ApiAddress));
        }

        public MainViewModel MainViewModel
        {
            get { return ServiceLocator.Current.GetInstance<MainViewModel>(); }
        }

        public StreetTabViewModel StreetTabViewModel
        {
            get { return ServiceLocator.Current.GetInstance<StreetTabViewModel>(); }
        }
        public StopTabViewModel StopTabViewModel
        {
            get { return ServiceLocator.Current.GetInstance<StopTabViewModel>(); }
        }

        public DriverTabViewModel DriverTabViewModel
        {
            get { return ServiceLocator.Current.GetInstance<DriverTabViewModel>(); }
        }

        public LineTabViewModel LineTabViewModel
        {
            get { return ServiceLocator.Current.GetInstance<LineTabViewModel>(); }
        }

        public VehicleTabViewModel VehicleTabViewModel
        {
            get { return ServiceLocator.Current.GetInstance<VehicleTabViewModel>(); }
        }

        public DatabaseTabViewModel DatabaseTabViewModel
        {
            get { return ServiceLocator.Current.GetInstance<DatabaseTabViewModel>(); }
        }

        public RideTabViewModel RideTabViewModel
        {
            get { return ServiceLocator.Current.GetInstance<RideTabViewModel>(); }
        }

        public ScheduleTabViewModel ScheduleTabViewModel
        {
            get { return ServiceLocator.Current.GetInstance<ScheduleTabViewModel>(); }
        }

        public LineHasStopTabViewModel LineHasStopTabViewModel
        {
            get { return ServiceLocator.Current.GetInstance<LineHasStopTabViewModel>(); }
        }

        public StatisticsTabViewModel StatisticsTabViewModel
        {
            get { return ServiceLocator.Current.GetInstance<StatisticsTabViewModel>(); }
        }

        public LineSearchByStopTabViewModel LineSearchByStopTabViewModel
        {
            get { return ServiceLocator.Current.GetInstance<LineSearchByStopTabViewModel>(); }
        }

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}