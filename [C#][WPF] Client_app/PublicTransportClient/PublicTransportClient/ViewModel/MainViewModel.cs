using System.Windows.Controls;
using GalaSoft.MvvmLight;
using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using RestSharp;

namespace PublicTransportClient.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(RestClient rc)
        {
            if (!IsInDesignMode)
            {
                rc.ReadWriteTimeout = 500;
                ChangedTabCommand = new RelayCommand<SelectionChangedEventArgs>(ChangedTab);
            }
        }

        private void ChangedTab(SelectionChangedEventArgs args)
        {
            if (args.AddedItems.Count == 1)
            { 
                TabItem ti = args.AddedItems[0] as TabItem;
                if (ti != null)
                    ((RCViewModelBase)((UserControl)ti.Content).DataContext).GetData();
            }
        }

        public ICommand ChangedTabCommand { get; set; }
    }
}