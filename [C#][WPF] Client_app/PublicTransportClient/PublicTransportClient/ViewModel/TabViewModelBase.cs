﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using RestSharp;
using System.Windows.Input;
using System.Windows.Controls;
using GalaSoft.MvvmLight.CommandWpf;
using PublicTransportClient.Tabs.Models;

namespace PublicTransportClient.ViewModel
{
    public abstract class TabViewModelBase<T> : RCViewModelBase where T : ModelBase
    {
        protected TabViewModelBase(RestClient rc, String apiPostfixUrl)
            : base(rc)
        {
            if (!IsInDesignMode)
            {
                ApiPostfixUrl = apiPostfixUrl;
                UpdateObjectCommand = new RelayCommand<DataGridRowEditEndingEventArgs>(UpdateData);
                DeleteObjectCommand = new RelayCommand<KeyEventArgs>(DeleteData);
            }
        }


        private T _SelectedObject;

        public T SelectedObject
        {
            get { return _SelectedObject; }
            set
            {
                _SelectedObject = value;
                RaisePropertyChanged(() => SelectedObject);
            }
        }

        private ObservableCollection<T> _Objects;

        public ObservableCollection<T> Objects
        {
            get { return _Objects; }
            set
            {
                _Objects = value;
                RaisePropertyChanged(() => Objects);
            }
        }


        public ICommand UpdateObjectCommand { get; private set; }
        public ICommand DeleteObjectCommand { get; private set; }

        public override void GetData()
        {
            var coll = GetCollection<T>(ApiPostfixUrl);
            if (coll != null)
                Objects = new ObservableCollection<T>(coll.OrderBy((i) => i.Id));
        }

        protected void UpdateData(DataGridRowEditEndingEventArgs args)
        {
            T newObject = (T)args.Row.Item;

            // PUT - update element
            if (!args.Row.IsNewItem)
            {
                PutObject(newObject);
                GetData();
            }
            // POST - add element
            else
            {
                if (newObject.IsValid())
                {
                    PostObject(newObject);
                    GetData();
                }
                // When user did not fill all required fields, delete the new row
                else
                    Objects.Remove(newObject);
            }
        }

        protected virtual void DeleteData(KeyEventArgs args)
        {
            if (args.Key != Key.Delete || (args.OriginalSource as DataGridCell) == null)
                return;
            
            var request = new RestRequest(ApiPostfixUrl + "/{id}", Method.DELETE);
            request.AddUrlSegment("id", SelectedObject.Id.ToString());
            request.RequestFormat = DataFormat.Json;

            RC.Execute(request);

            GetData();
        }
    }
}
