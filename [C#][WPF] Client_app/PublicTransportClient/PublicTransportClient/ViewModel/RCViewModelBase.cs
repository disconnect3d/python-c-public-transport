﻿using System;
using GalaSoft.MvvmLight;
using RestSharp;
using PublicTransportClient.Tabs.Models;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Messaging;
using PublicTransportClient.Common;

namespace PublicTransportClient.ViewModel
{
    public abstract class RCViewModelBase : ViewModelBase
    {
        protected RCViewModelBase(RestClient rc)
        {
            if (!IsInDesignMode)
                this.RC = rc;
        }

        public abstract void GetData();

        protected String ApiPostfixUrl { get; set; }

        protected ObservableCollection<T> GetCollection<T>(String apiPostfixUrl) where T : ModelBase
        {
            var request = new RestRequest(apiPostfixUrl, Method.GET);
            var response = RC.Execute<ObservableCollection<T>>(request);

            return response.Data;
        }

        protected IRestResponse PutObject<T>(T obj) where T : ModelBase
        {
            var putRequest = new RestRequest(ApiPostfixUrl + "/{id}", Method.PUT);
            putRequest.AddUrlSegment("id", obj.Id.ToString());
            putRequest.JsonizeObject(obj);

            var resp = RC.Execute<RestResponseDBError>(putRequest);

            if (resp.Data != null && resp.Data.Error != null)
                Messenger.Default.Send(resp.Data);

            return resp;
        }

        protected IRestResponse PostObject<T>(T obj) where T : ModelBase
        {
            var postRequest = new RestRequest(ApiPostfixUrl, Method.POST);
            postRequest.JsonizeObject(obj);

            var resp = RC.Execute<RestResponseDBError>(postRequest);

            if (resp.Data != null && resp.Data.Error != null)
                Messenger.Default.Send(resp.Data);
            
            return resp;
        }

        protected RestClient RC { get; private set; }
    }
}
