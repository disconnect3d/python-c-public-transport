﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace PublicTransportClient.Common
{
    public static class Extensions
    {
        public static void JsonizeObject(this IRestRequest  request, object obj)
        {
            request.RequestFormat = DataFormat.Json;
            request.JsonSerializer = new RestSharpJsonNetSerializer();
            request.AddJsonBody(obj);
        }
    }
}
