# Public transport project

The project has been done for a database university course on AGH University of Science and Technology.

**It has been done around October of 2014.**

The project involved mainly designing the database of public transport company. Instead of making a simple app that would use raw SQL queries I have done it **almost** like it should be done in real projects. Between the database and the client app, there is a REST API.

The missing part in the project is the REST API authentication, although there are cases in which its absence would not be a problem (just the company’s client user content).

Below I present flow diagram how the system works.

![docs.png](https://bitbucket.org/repo/ke9kdr/images/3151583980-docs.png)

And an overview of the C# client/desktop app:

![c.png](https://bitbucket.org/repo/ke9kdr/images/197690422-c.png)

Technologies used:

* C# WPF .NET 4.5.1
* MVVM design pattern (done with MVVM Light library)
* Python 3.4.x and Flask web microframework
* PostgreSQL database


Documentation (unfortunately in Polish): [/dokumentacja/db.pdf](https://bitbucket.org/disconnect3d/python-c-public-transport/raw/1b19976e8c45429e7de90104e71706696fb49ea0/dokumentacja/db.pdf)